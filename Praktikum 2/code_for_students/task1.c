#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#define DEBUG

/**
 * Merge arr1 of length n1 and arr2 of length n2 into
 * tmp_arr. This function expects array tmp_arr to be of
 * length n1 + n2
 */
void merge_arr( int* arr1, int n1, int* arr2, int n2,
                int* tmp_arr ) {
    int i = n1 + n2 - 1;
    n1--;
    n2--;
    while(i >= 0){
    // fill tmp_arr from behind, this allows the user to
    // choose tmp_arr to be the same array as arr1 or arr2
        if(n2 < 0 || (n1 >= 0 && arr1[n1] > arr2[n2])){
            tmp_arr[i] = arr1[n1];
            n1--;
        }
        else {
            tmp_arr[i] = arr2[n2];
            n2--;
        }
        i--;
    }
}

/**
* Compare two integers and return
* negative value:   if x < y
* positive value:   if x > y
* zero:             if x == y
*/
int compare(const void* x, const void* y){
    return *((const int*)x) - *((const int*)y);
}

/**
* Get the size of the array that needs to be allocated for this process
* size = min{ nprocs , max{2^i : myrank % 2^i == 0} } * orig_len
*/
int getSize(int myrank, int nprocs, int orig_len){
    if(myrank == 0){
        return nprocs * orig_len;
    }
    int exp2 = 2; // exp2 = 2^i
    while(myrank % exp2 == 0){ // will terminate before nprocs
        exp2*=2;
    }
    return (exp2/2) * orig_len;
}

/**
 * Do the parallel sorting
 */
void par_sort( int** orig_arr, int* orig_len, int myrank,
               int nprocs ) {

    /* Sort locally */
    qsort(*orig_arr, *orig_len, sizeof(int), compare);

    /* Allocate temp arrays */
    int new_len = getSize(myrank, nprocs, *orig_len);
    #ifdef DEBUG
    printf("\tp %i: newLen = %i\n", myrank, new_len);
    #endif // DEBUG
    int* array = malloc(new_len*sizeof(int));
    int* buffer = malloc((new_len/2)*sizeof(int));
    int i;
    for(i = 0; i < *orig_len; i++){
        array[i] = (*orig_arr)[i];
    }

    /* The loop repeats log2(nprocs) times */
    // Use merge_arr()
    int len = *orig_len;
    int exp2 = 2; //= 2^i

    MPI_Status* status; //needs to exist to please the compiler
    while(exp2 <= nprocs){
        if(myrank % exp2 == 0){
            // if processID is divisible by 2^i: receive array
            MPI_Recv(buffer, len, MPI_INT, myrank+exp2/2, exp2, MPI_COMM_WORLD, status);
            #ifdef DEBUG
            printf("\tp %i: rec[%i] from %i\n", myrank, len, myrank+exp2/2);
            #endif // DEBUG
            merge_arr(array, len, buffer, len, array);
        }
        else if (myrank % (exp2 / 2) == 0){
            // if processID is divisible by 2^(i-1) and not by 2^i: send array
            MPI_Send(array, len, MPI_INT, myrank-exp2/2, exp2, MPI_COMM_WORLD);
            #ifdef DEBUG
            printf("\tp %i: send[%i] to %i\n", myrank, len, myrank-exp2/2);
            #endif // DEBUG
        }
        else {
            // nothing left to do
            break;
        }
        len *= 2;
        exp2 *= 2;
    }

    /* Free memory */
    free(buffer);
    if(myrank == 0){
        free(*orig_arr);
        *orig_arr = array;
        *orig_len = nprocs * *(orig_len);
    }
    else {
        free(array);
    }
}

int is_arr_sorted( int* arr, int len ) {

    int i;
    for( i = 0; i < len - 1; ++i ){
    #ifdef DEBUG
    printf("%i\n", arr[i]);
    #endif // DEBUG
        if( arr[i] > arr[i+1] )
            return 0;
    }
    #ifdef DEBUG
    printf("%i\n(total length = %i)\n", arr[len-1], len);
    #endif // DEBUG
    return 1;
}

int main( int argc, char** argv ) {

    int myrank, nprocs;
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &myrank );
    MPI_Comm_size( MPI_COMM_WORLD, &nprocs );

    #ifdef DEBUG
    if(myrank == 0){
        printf("processors: %i\nargc: %i", nprocs, argc);
        if(argc > 1)
            printf(" argv=%s\n", argv[1]);
            else printf("\n");
    }
    printf("process %i at your service!\n", myrank);
    #endif // DEBUG

    /* Init the local part of the array */
    if( argc < 2 ) {
        printf( "Usage: %s <local_len>\n", argv[0] );
        exit( 1 );
    }
    int i, local_len = atoi( argv[1] );
    int* local_arr = malloc( local_len * sizeof(int) );

    /* Randomize the input */
    srand( time(NULL) * myrank );
    for( i = 0; i < local_len; ++i )
        local_arr[i] = rand();

    /* Parallel sort */
    par_sort( &local_arr, &local_len, myrank, nprocs );

    /* Verify the results */
    if( myrank == 0 ) {
        int res = is_arr_sorted( local_arr, local_len );
        printf( "Sorted: %d\n", res );
    }

    free( local_arr );
    #ifdef DEBUG
    printf("Process %i out!\n", myrank);
    #endif // DEBUG
    MPI_Finalize();

    return 0;
}
