#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include "timing.h"
//#define DEBUG

void print_arr( int* arr, int len ) {

    unsigned int i;
    for( i = 0; i < len; ++i )
        printf( "%d  ", arr[i] );
    printf("\n");
}

int is_arr_sorted( int* arr, int len ) {

    int i;
    for( i = 0; i < len - 1; ++i ){
    #ifdef DEBUG
    printf("%i\n", arr[i]);
    #endif // DEBUG
    if( arr[i] > arr[i+1] )
            return 0;
    }
    #ifdef DEBUG
    printf("%i\n\n\n", arr[len-1]);
    #endif // DEBUG
    return 1;
}

/**
* Compare two integers and return
* negative value:   if x < y
* positive value:   if x > y
* zero:             if x == y
*/
int compare(const void* x, const void* y){
    return *((const int*)x) - *((const int*)y);
}

/**
 * Determine the index of the beginning of each block
 * based on the global splitters
 */
void partition( int* data, int len, int* splitters,
                int num_sp, int* disp) {
    disp[0] = 0;
    int i;
    int counter = 0;
    for(i = 0; i < len; i++){
        if(counter == (num_sp)){
            break;
        }
        if(data[i] > splitters[counter]){
            disp[counter+1] = i;
            counter++;
        }
    }
}
/**
 * Verify that all data is sorted globally
 */
int verify_results_eff( int* arr, int len, int myrank, int nprocs ) {
    int send;
    #ifdef DEBUG
    printf("%i:\n", myrank);
    #endif // DEBUG
    if (is_arr_sorted(arr, len)){
        send = arr[0];
    } else {
        send = INT_MIN;
    }
    int result = INT_MAX;
    MPI_Status* status;
    if(myrank != nprocs-1){
        MPI_Recv(&result, 1, MPI_INT, myrank+1, 0, MPI_COMM_WORLD, status);
    }
    if(myrank != 0){
        if(arr[len-1] > result){
            send = INT_MIN;
        }
        MPI_Send(&send, 1, MPI_INT, myrank-1, 0, MPI_COMM_WORLD);
    }
    else {
        #ifdef DEBUG
        printf("send: %i, result: %i:\n", send, result);
        #endif // DEBUG
        if(arr[len-1] > result || send == INT_MIN){
            return 0;
        }
        return 1;
    }
    return 0;
}

/**
 * Do the parallel sorting
 *
 * Note: The length of the local input array may change in this
 *       call
 */
void par_sort( int** orig_arr, int* orig_len, int myrank,
               int nprocs ) {

    /* Sort locally */
     qsort(*orig_arr, *orig_len, sizeof(int), compare);

    /* Select local splitters */
    int* local_splitters = malloc( (nprocs-1) * sizeof(int) );
    int i;
    for(i = 0; i < (nprocs-1); i++){
        local_splitters[i] = (*orig_arr)[(((*orig_len)*(i+1))/nprocs)];
    }

    /* Gather all the splitters on the root process */
     int* all_loc_spl;
     if (myrank==0) {
        all_loc_spl = malloc((nprocs*(nprocs-1))*sizeof(int));
     }

     MPI_Gather(local_splitters, (nprocs-1), MPI_INT, all_loc_spl, (nprocs-1), MPI_INT, 0, MPI_COMM_WORLD);


    /* Select global splitters */
     int* global_splitters = malloc( (nprocs-1) * sizeof(int) );;
    if (myrank==0) {
        qsort(all_loc_spl, nprocs*(nprocs-1), sizeof(int), compare);
        for(i = 0; i < (nprocs-1); i++){
            global_splitters[i] = all_loc_spl[((nprocs-1)*(i+1))];
        }
    }

    MPI_Bcast(global_splitters, (nprocs-1), MPI_INT, 0, MPI_COMM_WORLD);

    /* Redistribute parts */
    // Call partition()
    // Tell each process how many elements I send to it
    // Allocate array of proper size
    int* disp = malloc( (nprocs) * sizeof(int) );
    partition(*orig_arr, *orig_len, global_splitters, (nprocs-1), disp);

    int* sendcounts = malloc( nprocs * sizeof(int) );
     for(i = (nprocs-1); i>=0; i--){
        if(i == (nprocs-1)){
            sendcounts[i] = (*orig_len) - disp[i];
        }
        else{
            sendcounts[i] = disp[i+1] - disp[i];
        }
    }

    int* reccounts = malloc(nprocs * sizeof(int));

    MPI_Alltoall(sendcounts, 1, MPI_INT, reccounts, 1, MPI_INT, MPI_COMM_WORLD);

    int* rdisp = malloc(nprocs * sizeof(int));
    int new_length = 0;
    for(i = 0; i < nprocs; i++){
        new_length += reccounts[i];
        if(i == 0){
            rdisp[i] = 0;
        }
        else{
            rdisp[i] = rdisp[i-1] + reccounts[i-1];
        }
    }
    int* recbuffer = malloc(new_length * sizeof(int));
    // Send one block to each process (blocks could also be empty)

    MPI_Alltoallv(*orig_arr, sendcounts, disp, MPI_INT, recbuffer, reccounts, rdisp, MPI_INT, MPI_COMM_WORLD);


    free(rdisp);
    free(reccounts);
    free(disp);
    free(sendcounts);
    free(local_splitters);
    free(global_splitters);
    if(myrank == 0){
        free(all_loc_spl);
    }

    /* Sort locally */
    qsort(recbuffer, new_length, sizeof(int), compare);

    free(*orig_arr);
    *orig_arr = recbuffer;
    *orig_len = new_length;
}

int main( int argc, char** argv ) {

    int myrank, nprocs;
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &myrank );
    MPI_Comm_size( MPI_COMM_WORLD, &nprocs );

    #ifdef DEBUG
    if(myrank == 0){
        printf("processors: %i\nargc: %i", nprocs, argc);
        if(argc > 1)
            printf(" argv=%s\n", argv[1]);
            else printf("\n");
    }
    printf("process %i at your service!\n", myrank);
    #endif // DEBUG
    init_clock_time ();

    /* Init the local part of the array */
    if( argc < 2 ) {
        printf( "Usage: %s <local_len>\n", argv[0] );
        exit( 1 );
    }
    int i, local_len = atoi( argv[1] );
    int* local_arr = malloc( local_len * sizeof(int) );

    /* Randomize the input */
    srand( time(NULL) * myrank );
    for( i = 0; i < local_len; ++i )
        local_arr[i] = rand();

    double start = get_clock_time();

    /* Parallel sort */
    par_sort( &local_arr, &local_len, myrank, nprocs );

    double elapsed = get_clock_time() - start;

    #ifdef DEBUG
    printf("Process %i finished sorting\n", myrank);
    #endif // DEBUG

    /* Verify the results */
    int res = verify_results_eff( local_arr, local_len, myrank, nprocs );
    if( myrank == 0 )
        printf( "Sorted: %d\n", res );

    /* Get timing - max across all ranks */
    double elapsed_global;
    MPI_Reduce( &elapsed, &elapsed_global, 1,
                MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD );
    if( myrank == 0 )
        printf( "Elapsed time (ms): %f\n", elapsed_global );

    free( local_arr );
    #ifdef DEBUG
    printf("Process %i out!\n", myrank);
    #endif // DEBUG
    MPI_Finalize();

    return 0;
}
